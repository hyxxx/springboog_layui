package com.haiyu.manager.service.test;

import java.util.Map;

import com.haiyu.manager.dto.test.TestSearchDTO;
import com.haiyu.manager.pojo.BaseAdminUser;
import com.haiyu.manager.response.PageDataResult;


/**
 * @Title: AdminUserService
 * @Description:
 * @author: youqing
 * @version: 1.0
 * @date: 2018/11/21 11:04
 */
public interface TestService {

    Map<String,Object> addU(BaseAdminUser user);

    Map<String,Object> update(BaseAdminUser user);

	PageDataResult getTestList(TestSearchDTO testSearchDTO, Integer pageNum,
			Integer pageSize);

}
