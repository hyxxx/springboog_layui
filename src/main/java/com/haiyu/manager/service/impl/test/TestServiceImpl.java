package com.haiyu.manager.service.impl.test;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.haiyu.manager.common.utils.DigestUtils;
import com.haiyu.manager.dto.AdminUserDTO;
import com.haiyu.manager.pojo.BaseAdminUser;
import com.haiyu.manager.pojo.test.TestData;
import com.haiyu.manager.dao.BaseAdminUserMapper;
import com.haiyu.manager.dto.UserSearchDTO;
import com.haiyu.manager.dto.test.TestSearchDTO;
import com.haiyu.manager.service.AdminUserService;
import com.haiyu.manager.service.test.TestService;
import com.haiyu.manager.response.PageDataResult;
import com.haiyu.manager.common.utils.DateUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title: AdminUserServiceImpl
 * @Description:
 * @author: youqing
 * @version: 1.0
 * @date: 2018/11/21 11:04
 */
@Service
public class TestServiceImpl implements TestService{

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

  //  @Autowired
   // private TestMapper testMapper;

    @Override
    public PageDataResult getTestList(TestSearchDTO testSearchDTO, Integer pageNum, Integer pageSize) {
     
    	List<Map<String, Object>> list=jdbcTemplate.queryForList("select * from test_data where test_input='175.24.183.11'");
    	System.out.println("输出："+list);
    	PageDataResult pageDataResult = new PageDataResult();
    	 PageHelper.startPage(pageNum, pageSize);
    	 if(list.size() != 0){
             PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(list);
             pageDataResult.setList(list);
             pageDataResult.setTotals((int) pageInfo.getTotal());
         }

         return pageDataResult;
    	/*  PageDataResult pageDataResult = new PageDataResult();
        List<TestData> listData = testMapper.getTestList(testSearchDTO);

        PageHelper.startPage(pageNum, pageSize);

        if(listData.size() != 0){
            PageInfo<TestData> pageInfo = new PageInfo<>(listData);
            pageDataResult.setList(listData);
            pageDataResult.setTotals((int) pageInfo.getTotal());
        }

        return pageDataResult;*/
    }

	@Override
	public Map<String, Object> addU(BaseAdminUser user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> update(BaseAdminUser user) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
