package com.haiyu.manager.pojo.test;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "test_data")
public class TestData  {
	
	@Column(name = "id")
	private String id;
	@Column(name = "test_input")
	private String testInput;		// 单行文本
	@Column(name = "test_textarea")
	private String testTextarea;		// 多行文本
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTestInput() {
		return testInput;
	}
	public void setTestInput(String testInput) {
		this.testInput = testInput;
	}
	public String getTestTextarea() {
		return testTextarea;
	}
	public void setTestTextarea(String testTextarea) {
		this.testTextarea = testTextarea;
	}
	
	
	
}