package com.haiyu.manager.controller.test;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.haiyu.manager.dto.LoginDTO;
import com.haiyu.manager.dto.UserSearchDTO;
import com.haiyu.manager.dto.test.TestSearchDTO;
import com.haiyu.manager.pojo.BaseAdminUser;
import com.haiyu.manager.response.PageDataResult;
import com.haiyu.manager.service.test.TestService;

@Controller
@RequestMapping("test")
public class Test2Controller {
	

	@Autowired
    private TestService testService;
	  /**
    *
    * 功能描述: 登入系统
    *
    * @param:
    * @return:
    * @auther: youqing
    * @date: 2018/11/22 15:47
    */
   @RequestMapping("test")
   @ResponseBody
   public Map<String,Object> test(HttpServletRequest request, LoginDTO loginDTO, HttpSession session){
	   System.out.println("test1");
	   return null;
   }
   
   @RequestMapping("testList")
   public String testList(){
	   System.out.println("testList11112");
       return "/test/testList";
   }
   
   /**
   *
   * 功能描述: 分页查询用户列表
   *
   * @param:
   * @return:
   * @auther: youqing
   * @date: 2018/11/21 11:10
   */
  @RequestMapping(value = "/getTestList", method = RequestMethod.POST)
  @ResponseBody
  public PageDataResult getTestList(@RequestParam("pageNum") Integer pageNum,
                                    @RequestParam("pageSize") Integer pageSize,/*@Valid PageRequest page,*/ TestSearchDTO testSearchDTO) {
      /*logger.info("分页查询用户列表！搜索条件：userSearch：" + userSearch + ",pageNum:" + page.getPageNum()
              + ",每页记录数量pageSize:" + page.getPageSize());*/
      PageDataResult pdr = new PageDataResult();
      try {
          if(null == pageNum) {
              pageNum = 1;
          }
          if(null == pageSize) {
              pageSize = 10;
          }
          // 获取用户列表
          pdr = testService.getTestList(testSearchDTO, pageNum ,pageSize);

      } catch (Exception e) {
          e.printStackTrace();
      }
      return pdr;
  }


  /**
   *
   * 功能描述: 新增和更新系统用户
   *
   * @param:
   * @return:
   * @auther: youqing
   * @date: 2018/11/22 10:14
   */
  @RequestMapping(value = "/setUser", method = RequestMethod.POST)
  @ResponseBody
  public Map<String,Object> setUser(BaseAdminUser user) {
      Map<String,Object> data = new HashMap();
      if(user.getId() == null){
       //   data = testService.add(user);
      }else{
          data = testService.update(user);
      }
      return data;
  }


  /**
   *
   * 功能描述: 删除/恢复 用户
   *
   * @param:
   * @return:
   * @auther: youqing
   * @date: 2018/11/22 11:59
   */
  @RequestMapping(value = "/updateUserStatus", method = RequestMethod.POST)
  @ResponseBody
  public Map<String, Object> updateUserStatus(@RequestParam("id") Integer id,@RequestParam("status") Integer status) {
    //  logger.info("删除/恢复用户！id:" + id+" status:"+status);
      Map<String, Object> data = new HashMap<>();
      if(status == 0){
          //删除用户
       //   data = adminUserService.delUser(id,status);
      }else{
          //恢复用户
        //  data = adminUserService.recoverUser(id,status);
      }
      return data;
  }
}
