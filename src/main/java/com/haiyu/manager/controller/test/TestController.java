package com.haiyu.manager.controller.test;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.haiyu.manager.dto.LoginDTO;
import com.haiyu.manager.service.AdminUserService;
import com.haiyu.manager.service.test.TestService;

@Controller
@RequestMapping("api")
public class TestController {
	
	  /**
    *
    * 功能描述: 登入系统
    *
    * @param:
    * @return:
    * @auther: youqing
    * @date: 2018/11/22 15:47
    */
   @RequestMapping("test")
   @ResponseBody
   public Map<String,Object> test(HttpServletRequest request, LoginDTO loginDTO, HttpSession session){
	   System.out.println("test");
	   return null;
   }
   
   @RequestMapping("home")
   public String home(){
       return "home";
   }
}
